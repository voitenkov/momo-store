# About the Project

The Momo Store App allows the user to order best dumplings ever! This application was developed to provide examples of how to build and support CI/CD process based on Gitlab CI and how to implement DevOps principles such as IaaC (Infrastructure As A Code) and DevOps instruments (Terraform, Argo CD).

<img width="900" alt="image" src="https://user-images.githubusercontent.com/9394918/167876466-2c530828-d658-4efe-9064-825626cc6db5.png">

See additional instructions and readme files:

for application in [https://gitlab.com/voitenkov/momo-store/-/tree/main/application](https://gitlab.com/voitenkov/momo-store/-/tree/main/application) repository.

for infrastructure in [https://gitlab.com/voitenkov/momo-store/-/tree/main/infrastructure](https://gitlab.com/voitenkov/momo-store/-/tree/main/infrastructure) repository.


## Project Technological Stack

- GO lang - for backend application.
- VUE framework - for frontend application.
- Docker containers
- Kubernetes orchestration


## Instruments Used in Project

- GitLab CI - to store application and infrastructure source code and to perform CI/CD process.
- SonarQube - source code analysis and quality assessment.
- Nexus - project artifacts repository.
- Yandex Cloud - to store project infrastructure and application.
- Terraform - initial deployment of development and production environments.
- Argo CD - Kubernetes applications CD.
- Prometheus - application metrics collection.
- Grafana - application observability.
- Alertmanager - alerting on application critical issues.


## Contributing

Any contributions you make are **greatly appreciated**.

If you have a suggestion that would make this better, please fork the repo and create a merge request. You can also simply open an issue with the tag "enhancement".

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Merge Request


## Contact

Andrey Voytenkov - avoytenkov@yandex.ru

Project Link: [https://gitlab.praktikum-services.ru/std-010-065/momo-store.git](https://gitlab.praktikum-services.ru/std-010-065/momo-store.git)
