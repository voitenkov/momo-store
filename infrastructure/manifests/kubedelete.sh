#!/bin/bash

kubectl delete ingress frontend
kubectl delete deployment frontend --cascade
kubectl delete deployment backend --cascade
kubectl delete service backend
kubectl delete service frontend
kubectl delete secret dockerlogin
kubectl delete configmap nginx-conf

