output "id" {
  description = "K8s cluster ID"
  value       = yandex_kubernetes_cluster.k8s-cluster.id
}