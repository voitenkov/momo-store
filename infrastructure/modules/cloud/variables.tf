variable "cloud_project" {
  type        = string
  description = "Name of project (cloud)"
}

variable "cloud_organization_id" {
  type        = string
  description = "YC Organization ID"
}

variable "cloud_billing_account_id" {
  type        = string
  description = "YC billing account ID"
}

