provider "yandex" {
  token     = var.token  
  cloud_id  = var.cloud_id
  
  # Not used here:
  # folder_id                = ""
  # service_account_key_file = ""
  # zone                     = ""
}