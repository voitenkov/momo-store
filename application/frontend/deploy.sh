#!/bin/bash
set +e

until sudo docker info; do sleep 1; done
docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY

docker-compose rm -f --stop momo-frontend
docker image prune -a -f
docker pull $CI_REGISTRY_IMAGE/mono-store-frontend:latest
docker-compose up -d momo-frontend
