# Application part of repository

## About the Project

Application consists of 2 microservices (frontend and backend) deployed in Yandex Cloud Managed Service for Kubernetes.

![Reference](/infrastructure/images/momo-app.jpg)


## Getting Started

Repository structure:
```
application
├── .gitlab-ci - CI/CD manifests
├── backend - backend application files
│   ├── cmd - source code for application (backend API server)
│   ├── internal - source code for application integration tests
│   ├── deploy.sh - shell script for application deploy to test virtual machine
│   ├── Dockerfile - backend container Dockerfile
│   ├── ... - some files need for go application build process
├── frontend - frontend application files
│   ├── public - public entities
│   ├── src - source code for application
│   ├── deploy.sh - shell script for application deploy to test virtual machine
│   ├── Dockerfile - frontend container Dockerfile
│   ├── nginx-service.conf - nginx web-server configuration file to be copied to frontend container
│   ├── ... - some files need for go application build process
├── .gitlab-ci.yml - main CI manifest
├── docker-compose.yml - Docker Compose file for local deploy/test
```

## Installation Instructions

These instructions demonstrate how to create the Momo Store App locally on your computer.


### Pre-requisites

If you would like to run the Momo Store App on your local machine, you will need to follow these instructions:

```sh
git clone git@gitlab.praktikum-services.ru:std-010-065/momo-store.git
```

### Installation

**For Backend:**

```sh
cd application/backend
go mod download
go build -a -o /app /app/...
```

**For Frontend:**

```sh
cd application/frontend
npm install
```

Additionally, you will need to export the `VUE_APP_API_URL` environment variable:

```sh
VUE_APP_API_URL=/api
```

The Vue tooling utilizes `npm` for its set of tools to help develop Vue applications.

Before starting, make sure that the following tools are installed on your computer:

* [Node](https://nodejs.org/en/)
* [npm (Node Package Manager)](https://www.npmjs.com)

Follow the installation instructions at [https://nodejs.org/en/](https://nodejs.org/en/).



## Running the Application

**For Backend:**

```sh
go run ./cmd/api
```


**For Frontend:**

### Run Development Server (with hot-reload)

```sh
npm run dev
```

### Compile and Minify for Production

```sh
npm run build
```
